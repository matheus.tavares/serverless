'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert('Users', [{
      firstName: 'matheus',
      lastName: 'accurate',
      email: 'ana@accurate.com.br',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      firstName: 'Bruno',
      lastName: 'accurate',
      email: 'cassio@accurate.com.br',
      createdAt: new Date(),
      updatedAt: new Date()
    }]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete('Users', null, {});
  }
};
