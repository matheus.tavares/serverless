const express = require('express')
//const pool = require('./config/db')
const serverless = require('serverless-http')
const bodyParser = require('body-parser')

const app = express()

app.use(bodyParser.json())
app.use(bodyParser.urlencoded({ extended: true }))

app.get('/users', async (queryInterface, Sequelize) => {
  console.log("Funfou")
  let conn;
  try {
      // establish a connection to MariaDB
      conn = queryInterface.get();

      const query = 'SELECT * FROM users'

      // execute the query and set the result to a new variable
      var rows = await conn.query(query);

      // return the results
      res.send(rows);
  } catch (err) {
      throw err;
  } finally {
      if (conn) return conn.release();
  }
});
module.exports.index = serverless(app)