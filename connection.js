const { Sequelize } = require('sequelize');
const dbStreamer = require('db-streamer'),

const sequelize = new Sequelize('database_development', 'root', 'admin', {
  dialect: 'mysql',
  dialectModule: dbStreamer,
  host: process.env.DB_HOST
})

module.exports = sequelize